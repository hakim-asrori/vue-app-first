import "./assets/input.css";
import "vue-toastification/dist/index.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";

import { createApp } from "vue";
import Toast from "vue-toastification";
import { abilitiesPlugin } from "@casl/vue";

import App from "./App.vue";
import router from "./router";
import store from "./store";
import abilities from "./store/services/abilities";

window.ability = abilities;
const options = {
  transition: "Vue-Toastification__bounce",
  maxToasts: 20,
  newestOnTop: true,
};

createApp(App)
  .use(router)
  .use(store)
  .use(Toast, options)
  .use(abilitiesPlugin, abilities)
  .mount("#app");
