import Home from "../pages/Home.vue";
import Post from "../pages/Post.vue";
import PostAdd from "../pages/admin/PostAdd.vue";
import PostDetail from "../pages/PostDetail.vue";
import Category from "../pages/Category.vue";
import User from "../pages/User.vue";
import auth from "./services/auth/";

const routes = [
  ...auth,
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/post",
    name: "Post",
    component: Post,
  },
  {
    path: "/post/add",
    name: "PostAdd",
    component: PostAdd,
  },
  {
    path: "/post/:slug",
    name: "PostDetail",
    component: PostDetail,
    props: true,
  },
  {
    path: "/category",
    name: "Category",
    component: Category,
  },
  {
    path: "/user",
    name: "User",
    component: User,
  },
];

export default routes;
